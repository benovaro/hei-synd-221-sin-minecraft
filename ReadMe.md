# Minecraft electrical age project
## Introduction

Le but de ce projet, à été de développer un software pour de l'automatisation industrielle. Nous avons utilisé comme système à contrôler un monde Minecraft (avec le mode electrcal age). Cependant, notre soft n'est pas dépendant de ce système et peut-être utilisé pour piloter n'importe quel automate programmable.
## Fonctionnement 

Notre programme va agir sur un Field (système contrôlé) en lui communiquant des paramètres enregistrés dans un fichier CSV. Ce fichier CVS contient toutes les données utiles au contrôle du monde Minecraft (label, adresse, ...). Il doit donc être mis à jour si le système à contrôler change.
Le framework va assurer la liaison entre une database, une interface web, le field et une instance de contrôle. Ainsi, lorsqu'une valeur est modifiée dans le système (par exemple un capteur qui change de valeur), la database, l'interface web et l'instance  de contrôle seront mis à jour. Et, a contrario, lorsque l'instance de contrôle décide d'un changement ou, lorsque l'on applique un changement depuis l'interface web, la data base est mise à jour  et les paramètres du système sont mis à jour.

Nous avons utilisé le protocole Modbus pour assurer la communication entre les différents éléments.

La classe SmartControll (notre instance de contrôle) est la seule classe du programme à être liée au système à contrôler. Toutes les autres font parties du framework qui peut être réutilisé dans n'importe quelle autre situation.

## Javadoc
Toute la documentation est disponible dans le dossier Javadoc présent dans les sources du projet. En cliquant sur la page web index, la Javadoc va s'afficher.
## Lancer le programme
Pour lancer le programme , il faut tout d'abord lancer la version test de Minecraft puis, il faut lancer le fichier Minecraft.jar que l'on peut trouver dans les sources du projet. Une fois le .jar lancé il faut entrer les paramètres de connection sous cette forme : <db_host> <db_name> <modbus_home> <modbus_port> [-modbus4j]. 
Pour lancer le .jar dans notre il suffit de copier la ligne suivante et de modifier le paramètre de modbus home si besoin : java -jar Minecraft.jar https://influx.sdi.hevs.ch/write?db=SIn13 SIn13 localhost 1502 -modbus4j

## Crédit

Développeurs : Pottier Guillaume & Varone Benoît & Gabioud Gaëlle

Sion, Lundi 29 mars 2021 .




