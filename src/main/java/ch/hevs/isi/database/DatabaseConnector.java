package ch.hevs.isi.database;
import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/**
 *This class create a connection to the database
 */
public class DatabaseConnector {
        private final static String url = "https://influx.sdi.hevs.ch/write?db=SIn13";
        private final static String username = "SIn13";
        private final static String password = "707fedb0aaf63c78e448d14c631b54c6";
        private final static String db = "SIn13";
        private static DatabaseConnector theDbC=null;

    /**
     *Instance
     */
    public static DatabaseConnector getInstance() {
        if(theDbC==null){
            theDbC = new DatabaseConnector();
        }
        return theDbC;
    }

    /**
     * Constructor of the class
     */
    private DatabaseConnector(){
    }

    /**
     * Ask the value and the label of the floatdatapoint to send it to the database
     * @param fdp Float datapoint to push on the database
     */
    public void onNewValue(FloatDataPoint fdp) {

        pushToDatabase(fdp.getLabel(),String.valueOf(fdp.getValue()));
    }
    /**
     * Ask the value and the label of the BinaryDataPoint to send it to the database
     * @param bdp BinaryDataPoint to push on the database
     */
    public void onNewValue(BinaryDataPoint bdp) {

        pushToDatabase(bdp.getLabel(),String.valueOf(bdp.getValue()));
    }

    /**
     * This method push to Database
     * @param label Label to push on the database
     * @param value Value to push on the database
     */
    private void pushToDatabase(String label, String value) {
        //Check the exception
        try {
            //Configure url
            URL myurl = new URL(url);
            HttpURLConnection urlConnection = null;
            String userpass = username + ":" + password;
            String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
            urlConnection = (HttpURLConnection) myurl.openConnection();
            urlConnection.setRequestProperty("Authorization", "Basic " + encoding);
            urlConnection.setRequestProperty("Content-Type", "binary/octet-stream");
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            //Write
            OutputStreamWriter osWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            System.out.println(label + " value=" + value);
            osWriter.write(label + " value=" + value);
            osWriter.flush();
            int reponsecode = urlConnection.getResponseCode();
            System.out.println(reponsecode);
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));


            //Check the status of the reponse
        if (reponsecode == 204) {
            while ((in.readLine()) != null) {
            }
        }
        urlConnection.disconnect();
        }catch (IOException e) {
            e.printStackTrace();
            System.out.println("DataBase is failed");
        }
    }
        /**
         *Main
         */
        public static void main (String[] args) {

        DatabaseConnector dc = DatabaseConnector.getInstance();

            while(true){
                dc.pushToDatabase("Mesure", "5");
                Utility.waitSomeTime(1000);
                dc.pushToDatabase("Mesure", "80");
                Utility.waitSomeTime(1000);
                dc.pushToDatabase("Mesure", "46");
                Utility.waitSomeTime(1000);
            }

        }

    }

