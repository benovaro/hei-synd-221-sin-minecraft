package ch.hevs.isi.SmartControl;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import java.util.TimerTask;

/**
 *This class optimis the process
 */
public class SmartControl extends TimerTask {

    FloatDataPoint factoryPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
    FloatDataPoint solarPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
    FloatDataPoint windPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("WIND_P_FLOAT");
    FloatDataPoint coalPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_P_FLOAT");
    BinaryDataPoint remoteSolar = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
    BinaryDataPoint remoteWind = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
    FloatDataPoint coalST = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
    FloatDataPoint batteryLevel = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
    FloatDataPoint coalAmount = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_AMOUNT");
    FloatDataPoint time = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("CLOCK_FLOAT");

    /**
     * This method is launched periodically by the TimerTask
     */
    @Override
    public void run() {
        if (coalAmount.getValue() > 0) {

            //Night
            if (time.getValue() > 0.75f || time.getValue() <= 0.25f) {
                factoryPower.setValue(0f);
                if (batteryLevel.getValue() < 0.4f) {
                    coalST.setValue(0.9f);
                } else {
                    coalST.setValue(0.6f);
                }
            }

            //Day
            else {
                if (batteryLevel.getValue() >= 0.85f && batteryLevel.getValue() < 1f) {
                    remoteWind.setValue(true);
                    remoteSolar.setValue(true);
                    coalST.setValue(0f);
                    factoryPower.setValue(1f);
                } else if (batteryLevel.getValue() < 0.85f) {
                    remoteWind.setValue(true);
                    remoteSolar.setValue(true);
                    coalST.setValue((float) (1 - batteryLevel.getValue()));
                    if ((solarPower.getValue() + windPower.getValue() + coalPower.getValue()) < 0.45f) {
                        factoryPower.setValue(0f);
                    } else {
                        if (batteryLevel.getValue() > 0.4f) {
                            factoryPower.setValue((float) (2.5 * batteryLevel.getValue() - 1));
                        }
                    }

                }
            }
        }

        if (coalAmount.getValue() == 0f) {
            if (batteryLevel.getValue() >= 0.6f) {
                remoteWind.setValue(true);
                remoteSolar.setValue(true);
                coalST.setValue(0f);
                factoryPower.setValue(0.5f);
            } else if (batteryLevel.getValue() <= 0.6f) {
                remoteWind.setValue(true);
                remoteSolar.setValue(true);
                coalST.setValue(0f);
                factoryPower.setValue(0f);
            }
        }
    }
}


