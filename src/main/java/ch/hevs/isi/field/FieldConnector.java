package ch.hevs.isi.field;
import ch.hevs.isi.core.*;
import ch.hevs.isi.utils.Utility;


import java.util.Timer;


/**
 * This class is used to create a connection to the field
 */
public class FieldConnector {
    private static FieldConnector fc = new FieldConnector();

    /**
     * Constructor
     */
    private FieldConnector (){

        Timer pollTimer = new Timer();
        pollTimer.scheduleAtFixedRate(new PollTask(), 0, 3000);

    }

    /**
     * pushToField ->
     *
     * @param dp
     * @param isBoolean
     */
    private void pushToField(DataPoint dp, boolean isBoolean){

            if (isBoolean) {
                BooleanRegister booleanR = BooleanRegister.getRegisterFromDataPoint((BinaryDataPoint) dp);
                System.out.println("Label : " + dp.getLabel() + " / Value : " + ((BinaryDataPoint) dp).getValue());
                booleanR.write();
            } else {
                FloatRegister floatR = FloatRegister.getRegisterFromDataPoint((FloatDataPoint) dp);
                System.out.println("Label : " + dp.getLabel() + " / Value : " + ((FloatDataPoint) dp).getValue());
                floatR.write();
            }
    }



    /**
     * Ask the value and the label of the BooleanDataPoint to send it to the field
     * @param datapoint boolean datapoint to push on the field
     */
    public void onNewValue(BinaryDataPoint datapoint) {
        System.out.println("Label : " + datapoint.getLabel() + " / Value : " + datapoint.getValue());
        BooleanRegister br = BooleanRegister.getRegisterFromDataPoint(datapoint);
        br.write();
    }



    /**
     * Ask the value and the label of the FloatDataPoint to send it to the field
     * @param datapoint float datapoint to push on the field
     */
    public void onNewValue(FloatDataPoint datapoint) {
        System.out.println("Label : " + datapoint.getLabel() + " / Value : " + datapoint.getValue());
        FloatRegister fr = FloatRegister.getRegisterFromDataPoint(datapoint);
        fr.write();

    }
    /**
     * Get instance of a field connector
     *
     * @return fc
     */
    public static FieldConnector getInstance(){

        return fc;
    }

    public static void main(String[] args){
        FloatRegister a = new FloatRegister("REMOTE_COAL_SP", true, 209, 1, 0);
        a.read();
        BooleanRegister b = new BooleanRegister("REMOTE_SOLAR_SW", true, 401);
        b.read();

    }
}

