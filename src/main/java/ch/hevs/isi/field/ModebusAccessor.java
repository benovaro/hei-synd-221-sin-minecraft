package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

/**
 * This class is singleton who does the modbus connection
 */
public class ModebusAccessor {

    private static ModebusAccessor instance = null;
    ModbusMaster master;
    ModbusFactory modbusFactory = new ModbusFactory();

    private String host;
    private int port;

    /**
     * ModebusAccessor ->   the constructor sets the host and port value and call the
     * connect method to start the a connection with the desired
     * port number
     *
     * @param host
     * @param port
     */
    public ModebusAccessor(String host, int port)  {

        this.host = host;
        this.port = port;

        connect(true);

    }

    /**
     * getInstance ->   create an instance with a default host name (localhost and a
     * port number (1502)
     *
     * @return an instance
     * @throws ModbusInitException
     */
    public static ModebusAccessor getInstance()  {
        if (instance == null) {
            instance = new ModebusAccessor("localhost", 1502);
        }

        return instance;

    }

    /**
     * getInstance -> create an instance with a given host name and port number
     *
     * @param host
     * @param port
     * @return an instance
     * @throws ModbusInitException
     */
    public static ModebusAccessor getInstance(String host, int port) throws ModbusInitException {
        if (instance == null) {
            instance = new ModebusAccessor(host, port);
        }

        return instance;

    }



    /**
     * connect ->   set ip parameters (host and port) than create a tcp master
     * and start a modbus connection
     *
     * @param state (true = keep the connection alive, false = kill the connection)
     * @throws ModbusInitException
     */
    public void connect(Boolean state) {

        IpParameters param = new IpParameters();

        param.setHost(host);
        param.setPort(port);

        master = modbusFactory.createTcpMaster(param, state);

        try {
            master.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
            System.out.println("An exception has been found " + "in the connect methode (ModebusAccessor class)");
        }
    }

    /**
     * readFloat -> return a float value
     *
     * @param regAdress
     * @return
     */
    public Float readFloat(int regAdress) {
        try {
            // getting the Float value
            return (float) master.getValue(BaseLocator.inputRegister(1, regAdress, DataType.FOUR_BYTE_FLOAT));
        } catch (ErrorResponseException | ModbusTransportException e) {
            e.printStackTrace();
            System.out.println("An exception has been found " + "in the readFloat methode (ModebusAccessor class)");
        }
        //in case nothing has been found
        return null;
    }

    /**
     * writeFloat -> write a float value
     *
     * @param regAdress
     * @param newValue
     * @throws ErrorResponseException
     * @throws ModbusTransportException
     */
    public void writeFloat(int regAdress, float newValue) {
        try {
            master.setValue(BaseLocator.holdingRegister(1, regAdress, DataType.FOUR_BYTE_FLOAT), newValue);
        } catch (ErrorResponseException | ModbusTransportException e) {
            e.printStackTrace();
            System.out.println("An exception has been found " + "in the writeFloat methode (ModebusAccessor class)");
        }
    }

    /**
     * readBoolean -> return a Boolean value
     *
     * @param regAdress
     * @return
     */
    public Boolean readBoolean(int regAdress) {
        try {
            // getting the Boolean value
            return master.getValue(BaseLocator.coilStatus(1, regAdress));
        } catch (ErrorResponseException | ModbusTransportException e) {
            e.printStackTrace();
            System.out.println("An exception has been found " + "in the readBoolean methode (ModebusAccessor class)");
        }

        //in case nothing has been found
        return null;
    }

    /**
     * writeBoolean -> write a boolean value
     *
     * @param regAdress
     * @param newValue
     * @throws ErrorResponseException
     * @throws ModbusTransportException
     */
    public void writeBoolean(int regAdress, Boolean newValue) {
        try {
            master.setValue(BaseLocator.coilStatus(1, regAdress), newValue);
        } catch (ErrorResponseException | ModbusTransportException e) {
            e.printStackTrace();
            System.out.println("An exception has been found " + "in the writeFloat methode (ModebusAccessor class)");
        }
    }

    /**
     * main -> this main is used to test the ModebusAccessor class
     *
     * @param args
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     * @throws ModbusInitException
     */
    public static void main(String[] args) throws ModbusInitException {
        ModebusAccessor mbAcces = ModebusAccessor.getInstance("localhost", 1502);


        boolean running = true;

        while (running) {

            /*
                Read float
                CMD : remote_Factory_Sp
                Address : 205
             */
            Float remoteFactorySp = mbAcces.readFloat(205);

            //cheking if a value has been returned
            if (remoteFactorySp == null) {
                Utility.DEBUG("ModebusAccessor", "main", "Modebus connection error");
                running = false;
            } else {
                Utility.DEBUG("ModebusAccessor", "main", "Registre 205 " + remoteFactorySp);
                Utility.waitSomeTime(1000);
            }

             /*
                Write float
                CMD : remoteFactorySp
                Address : 401
             */
            mbAcces.writeFloat(205, 0.12f);

             /*
                Read Boolean
                CMD : remote_Solar_Sw
                Address : 401
             */
            Boolean remoteSolarSw = mbAcces.readBoolean(609);

            //cheking if a value has been returned
            if (remoteSolarSw == null) {
                Utility.DEBUG("ModebusAccessor", "main", "Modebus connection error");
                running = false;
            } else {
                Utility.DEBUG("ModebusAccessor", "main", "Registre 609 " + remoteSolarSw);
                Utility.waitSomeTime(1000);
            }

            //write Boolean
            mbAcces.writeBoolean(401, !remoteSolarSw);
        }
    }
}


