package ch.hevs.isi.core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


/**
 * This class  test of the functionality of the core
 */
public class Launcher {

    public void launcher(){
        String line = "";
        String separateur = ";";
        try{
            BufferedReader br = new BufferedReader(new FileReader("ModbusEA_SIn.csv"));
            br.readLine();

            while((line = br.readLine())!= null){
                String [] tab = line.split(separateur);
                //System.out.println("Label= " + tab[0] + "boolean=" + tab[1] + "isOutput"+ tab[2] +"Address" + tab[3] + "Range=" + tab[4] + "Offset=" + tab[5]);
                if(tab[1].equalsIgnoreCase("true")){
                    BooleanRegister booleanr = new BooleanRegister(tab[0], Boolean.parseBoolean(tab[2]), Integer.parseInt(tab[3]));
                }
                else{
                    FloatRegister floatr = new FloatRegister(tab[0], Boolean.parseBoolean(tab[2]), Integer.parseInt(tab[3]), Integer.parseInt(tab[4]), Integer.parseInt(tab[5]));
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
