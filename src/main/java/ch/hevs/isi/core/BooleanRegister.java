package ch.hevs.isi.core;

import ch.hevs.isi.field.ModebusAccessor;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import java.util.HashMap;

/**
 * This class manage the boolean register
 */
public class BooleanRegister {
    int address;
    private BinaryDataPoint bdp;
    private static HashMap<DataPoint,BooleanRegister > hm = new HashMap<>();

    /**
     * Constructor
     * @param Label Label of the register
     * @param isOutput Type of the register (input or output)
     */
    public BooleanRegister(String Label, boolean isOutput, int address){
        this.address = address;
        bdp = new BinaryDataPoint(Label, isOutput);
        hm.put(bdp,this );
    }

    /**
     *  read() -> read the value in the register and set it in the BinaryDataPoint object
     */
    public void read() {
            boolean newValue =  ModebusAccessor.getInstance().readBoolean(address);
            bdp.setValue(newValue);
    }



    /**
     *  write -> write a value on the register
     */
    public void write () {
            boolean value = bdp.getValue();
            ModebusAccessor.getInstance().writeBoolean(address, value);
    }
    /**
     * poll() ->
     *
     * @throws ModbusInitException
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public static void poll(){
        // this loop read all the Hmap's value
        for (BooleanRegister br : hm.values()) {
            br.read();
        }
    }



    /**
     * booleanRegister() ->
     *
     * @param bdp
     * @return a BooleanRegister
     */
    public static BooleanRegister getRegisterFromDataPoint(BinaryDataPoint bdp) {
        BooleanRegister br = hm.get(bdp);
        return br ;
    }
}
