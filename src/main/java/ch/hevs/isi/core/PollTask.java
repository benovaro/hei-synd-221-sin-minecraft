package ch.hevs.isi.core;

import java.util.TimerTask;


/**
 * This class run the register
 */
public class PollTask extends TimerTask {

    @Override
    public void run(){
        BooleanRegister.poll();
        FloatRegister.poll();
    }
}

