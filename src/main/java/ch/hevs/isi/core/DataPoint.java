package ch.hevs.isi.core;


import java.util.HashMap;

/**
 * This class create a datapoint who associate a value to a label
 */
public abstract class DataPoint {

    private static HashMap<String, DataPoint> hm = new HashMap<>();
    private String label;
    private boolean isOutput;

    /**
     * Constructor
     * @param label label of the datapoint
     * @param isOutput Type of the datapoint
     */
    DataPoint(String label, boolean isOutput) {
        this.isOutput = isOutput;
        this.label = label;
        hm.put(label, this);
    }

    /**
     * @return a datapoint from the research label on the maps
     */
    public static DataPoint getDataPointFromLabel(String label) {

        return hm.get(label);
    }

    /**
     * @return the label of the datapoint
     */
    public String getLabel() {

        return label;
}

    /**
     * @return isOutput of the datapoint
     */
    public boolean getIsOutput() {

        return isOutput;
    }

}
