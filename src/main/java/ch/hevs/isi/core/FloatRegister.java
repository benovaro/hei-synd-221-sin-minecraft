package ch.hevs.isi.core;

import ch.hevs.isi.field.ModebusAccessor;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import java.util.HashMap;



/**
 * This class manage the float register
 */
public class FloatRegister {

    int address;
    int offset;
    private FloatDataPoint fdp;
    int range;
    private static HashMap<DataPoint,FloatRegister > hm = new HashMap<>();



    /**
     * Constructor
     *
     * @param Label Label of the register
     * @param isOutput Type of the register (input or output)
     */
    public FloatRegister(String Label, boolean isOutput, int address, int range, int offset){
        this.address = address;
        this.range = range;
        this.offset = offset;
        fdp = new FloatDataPoint(Label, isOutput, range);
        hm.put(fdp, this);
    }



    /**
     * read() -> read the value in the register and set it in the FloatDataPoint object
     */
    public void read() {
            float newValue = ModebusAccessor.getInstance().readFloat(address);
            fdp.setValue(newValue);
    }



    /**
     *  write -> write a value on the register
     */
    public void write () {
            float value = fdp.getValue();
            ModebusAccessor.getInstance().writeFloat(address, value);
    }



    /**
     * poll() ->
     *
     * @throws ModbusInitException
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public static void poll(){
        // boucle qui read() toutes les valeurs de hashmap
        for (FloatRegister fr : hm.values()) {
            fr.read();
        }
    }

    /**
     * FloatRegister() ->
     *
     * @param Floatdp
     * @return
     */
    public static FloatRegister getRegisterFromDataPoint(FloatDataPoint Floatdp) {
        FloatRegister floatR = hm.get(Floatdp);
        return floatR;
    }
}
