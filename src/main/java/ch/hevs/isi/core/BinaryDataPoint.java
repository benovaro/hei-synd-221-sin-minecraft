package ch.hevs.isi.core;


import ch.hevs.isi.field.FieldConnector;


/**
 * This class modify the Datapoint class to work with boolean
 */

public class BinaryDataPoint extends DataPoint {

    boolean value;

    /**
     * Constructor
     * @param label label of the datapoint
     * @param isOutput Type of the datapoint
     */
    public BinaryDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Update the value
     */
    public void setValue(boolean newValue) {

        if(newValue != value) {
            this.value = newValue;
          /*DatabaseConnector.getInstance().onNewValue(this);
            WebConnector.getInstance().onNewValue(this);*/
            if (getIsOutput())
                FieldConnector.getInstance().onNewValue(this);
        }
    }

    /**
     * @return the value
     */
    public boolean getValue(){

        return value ;
    }

}
