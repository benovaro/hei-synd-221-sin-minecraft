package ch.hevs.isi.core;
import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;


/**
 * This class modify the Datapoint class to work with float
 */
public class FloatDataPoint extends DataPoint {

    private float value ;
    private int range;

    /**
     * Constructor
     * @param label label of the datapoint
     * @param isOutput Type of the datapoint
     */
    public FloatDataPoint(String label, boolean isOutput, int range) {

        super(label, isOutput);
        this.range = range;
    }

    /**
     * Update the value
     */
    public void setValue(float value) {
    this.value = value ;
    DatabaseConnector dbc = DatabaseConnector.getInstance();
    dbc.onNewValue(this);
    WebConnector wbc = WebConnector.getInstance();
    wbc.onNewValue(this);
    FieldConnector fc = FieldConnector.getInstance();
    fc.onNewValue(this);
    }

    /**
     * @return the value
     */
    public float getValue(){

        return value;
    }
}
