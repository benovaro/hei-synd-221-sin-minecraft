package ch.hevs.isi.web;
import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;
import java.util.ArrayList;

/**
 *This class create a connection to the Webpage
 */
public class WebConnector extends WebSocketServer {
    private static WebConnector theWbc = new WebConnector();
    ArrayList<WebSocket> websocketList = new ArrayList<>();

    /**
     * Constructor of the class
     */
    private WebConnector(){
        super(new InetSocketAddress(8888));
        this.start();
    }
    //Open a connection
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        websocketList.add(webSocket);
        webSocket.send("Welcome a client has connected");
    }

    //Close a connection
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        websocketList.remove(webSocket);
        webSocket.send("a client has disconnected");
    }

    //You receive a message from the web page
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        String [] message = s.split("=");
        String label = message[0];
        String value = message[1];

       if(value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")){
           BinaryDataPoint bdp = (BinaryDataPoint) DataPoint.getDataPointFromLabel(label);
           bdp.setValue(Boolean.valueOf(value));
           System.out.println(bdp.getLabel() + ":" + bdp.getValue());

       }

       else{
           FloatDataPoint fdp = (FloatDataPoint) DataPoint.getDataPointFromLabel(label);
           fdp.setValue(Float.valueOf(value));
       }
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
    }

    @Override
    public void onStart() {
    }

    /**
     * This method push to Webpage
     * @param label Label to push on the webpage
     * @param value Value to push on the webpage
     */
    private void pushToWebPages(String label, String value){
        for(int i = 0; i<websocketList.size() ; i++){
            websocketList.get(i).send(label + "=" + value);
        }
       System.out.println("Le label : " + label + ", La valeur : " + value);
    }

    /**
     * Ask the value and the label of the floatDatapoint to send it to the database
     * @param fdp FloatDatapoint to push on the database
     */
    public void onNewValue(FloatDataPoint fdp){
        pushToWebPages(fdp.getLabel(),Float.toString(fdp.getValue()));
    }

    /**
     * Ask the value and the label of the BinaryDataPoint to send it to the database
     * @param bdp BinaryDatPoint to push on the database
     */
    public void onNewValue(BinaryDataPoint bdp){
        boolean value = bdp.getValue();
        pushToWebPages(bdp.getLabel(),String.valueOf(bdp.getValue()));
    }

    /**
     *Instance
     */
    public static WebConnector getInstance() {
        if(theWbc==null){
            theWbc = new WebConnector();
        }
        return theWbc;
    }

    /**
     *Main
     */
    public static void main(String[] args) {

        new WebConnector().getInstance();
        BinaryDataPoint bdp1 = new BinaryDataPoint("REMOTE_SOLAR_SW",false );
        BinaryDataPoint bdp2 = new BinaryDataPoint("REMOTE_WIND_SW",false );
        FloatDataPoint fdp1 = new FloatDataPoint("REMOTE_FACTORY_SP", false, 1);
    }

}
